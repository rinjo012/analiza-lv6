﻿namespace LV6_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Unesi = new System.Windows.Forms.Button();
            this.btn_Exit = new System.Windows.Forms.Button();
            this.lbl_BrojPokusaja = new System.Windows.Forms.Label();
            this.lbl_Rijec = new System.Windows.Forms.Label();
            this.tB_Unos = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_Unesi
            // 
            this.btn_Unesi.Location = new System.Drawing.Point(32, 94);
            this.btn_Unesi.Name = "btn_Unesi";
            this.btn_Unesi.Size = new System.Drawing.Size(75, 23);
            this.btn_Unesi.TabIndex = 0;
            this.btn_Unesi.Text = "Unesi";
            this.btn_Unesi.UseVisualStyleBackColor = true;
            this.btn_Unesi.Click += new System.EventHandler(this.btn_Unesi_Click);
            // 
            // btn_Exit
            // 
            this.btn_Exit.Location = new System.Drawing.Point(32, 154);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.Size = new System.Drawing.Size(75, 23);
            this.btn_Exit.TabIndex = 1;
            this.btn_Exit.Text = "Exit";
            this.btn_Exit.UseVisualStyleBackColor = true;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // lbl_BrojPokusaja
            // 
            this.lbl_BrojPokusaja.AutoSize = true;
            this.lbl_BrojPokusaja.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_BrojPokusaja.Location = new System.Drawing.Point(248, 51);
            this.lbl_BrojPokusaja.Name = "lbl_BrojPokusaja";
            this.lbl_BrojPokusaja.Size = new System.Drawing.Size(58, 29);
            this.lbl_BrojPokusaja.TabIndex = 2;
            this.lbl_BrojPokusaja.Text = "Broj";
            // 
            // lbl_Rijec
            // 
            this.lbl_Rijec.AutoSize = true;
            this.lbl_Rijec.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Rijec.Location = new System.Drawing.Point(248, 106);
            this.lbl_Rijec.Name = "lbl_Rijec";
            this.lbl_Rijec.Size = new System.Drawing.Size(70, 29);
            this.lbl_Rijec.TabIndex = 3;
            this.lbl_Rijec.Text = "Rijec";
            this.lbl_Rijec.Click += new System.EventHandler(this.lbl_Rijec_Click);
            // 
            // tB_Unos
            // 
            this.tB_Unos.Location = new System.Drawing.Point(32, 39);
            this.tB_Unos.Name = "tB_Unos";
            this.tB_Unos.Size = new System.Drawing.Size(169, 22);
            this.tB_Unos.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 216);
            this.Controls.Add(this.tB_Unos);
            this.Controls.Add(this.lbl_Rijec);
            this.Controls.Add(this.lbl_BrojPokusaja);
            this.Controls.Add(this.btn_Exit);
            this.Controls.Add(this.btn_Unesi);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Unesi;
        private System.Windows.Forms.Button btn_Exit;
        private System.Windows.Forms.Label lbl_BrojPokusaja;
        private System.Windows.Forms.Label lbl_Rijec;
        private System.Windows.Forms.TextBox tB_Unos;
    }
}

