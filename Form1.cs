﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV6_2
{
    public partial class Form1 : Form
    {
        string path = "C:\\Users\\Bruno\\Desktop\\New folder\\vjesalo.txt";
        Random rand = new Random();
        List<string> list = new List<string>();
        int brojPokusaja;
        string RijecIzListe, RijecULabelu;
        public void Reset()
        {
            RijecIzListe = list[rand.Next(0, list.Count - 1)];
            RijecULabelu = new string('*', RijecIzListe.Length);
            lbl_Rijec.Text = RijecULabelu;
            brojPokusaja = 6;
            lbl_BrojPokusaja.Text = brojPokusaja.ToString();
        }
        public Form1()
        {
            InitializeComponent();
        }
        

        private void Form1_Load_1(object sender, EventArgs e)
        {
            string line;
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))

            {
                while ((line = reader.ReadLine()) != null)
                {
                    list.Add(line);
                }
                
            }
            Reset();
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lbl_Rijec_Click(object sender, EventArgs e)
        {

        }

        private void btn_Unesi_Click(object sender, EventArgs e)
        {
            if (tB_Unos.Text.Length == 1)
            {
                if (RijecIzListe.Contains(tB_Unos.Text))
                {
                    string rijec = RijecIzListe;
                    while (rijec.Contains(tB_Unos.Text))
                    {
                        int index = rijec.IndexOf(tB_Unos.Text);
                        StringBuilder Rijec = new StringBuilder(rijec);
                        Rijec[index] = '-';
                        rijec = Rijec.ToString();
                        StringBuilder Label = new StringBuilder(RijecULabelu);
                        Label[index] = Convert.ToChar(tB_Unos.Text);
                        RijecULabelu = Label.ToString();
                    }
                    lbl_Rijec.Text = RijecULabelu;
                    if (RijecULabelu == RijecIzListe)
                    {
                        MessageBox.Show("You Win!");
                        tB_Unos.Text = String.Empty;
                        Reset();
                    }
                }
                else
                {
                    brojPokusaja--;
                    lbl_BrojPokusaja.Text = brojPokusaja.ToString();
                    if (brojPokusaja == 0)
                    {
                        MessageBox.Show("Loser!");
                        tB_Unos.Text = String.Empty;
                        Reset();
                    }
                    
                }
            }
            else if (tB_Unos.Text.Length > 1)
            {
                if (RijecIzListe == tB_Unos.Text)
                {
                    lbl_Rijec.Text = tB_Unos.Text;
                    MessageBox.Show("You Win!");
                    tB_Unos.Text = String.Empty;
                    Reset();
                }
                else
                {
                    brojPokusaja--;
                    lbl_BrojPokusaja.Text = brojPokusaja.ToString();
                    if (brojPokusaja == 0)
                    {
                        MessageBox.Show("Loser!");
                        tB_Unos.Text = String.Empty;
                        Reset();

                    }
                }
            }
        }
    }
}

